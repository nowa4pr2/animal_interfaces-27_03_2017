package at.campus02.nowa.model;

public class Frog extends Animal {

	public Frog(String name, String color, int countEyes) {
		super(name, color, countEyes);
	}

	@Override
	public void walk() {
		System.out.println("run run run");

	}

	@Override
	public void makeNoise() {
		System.out.println("quak, quak, quak  ...");

	}

	@Override
	public String toString() {
		return "Bird [getName()=" + getName() + ", getColor()=" + getColor() + ", getCountEyes()=" + getCountEyes()
				+ "]";
	}
}
