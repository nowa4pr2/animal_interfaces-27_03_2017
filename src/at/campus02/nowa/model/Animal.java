package at.campus02.nowa.model;

public abstract class Animal {
	private String name;
	private String color;
	private int countEyes;
	
	public Animal (String name, String color, int countEyes) {
		this.name = name;
		this.color = color;
		this.countEyes = countEyes;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getCountEyes() {
		return countEyes;
	}

	public void setCountEyes(int countEyes) {
		this.countEyes = countEyes;
	}

	public abstract void walk();
	
	public abstract  void makeNoise();

	
}
