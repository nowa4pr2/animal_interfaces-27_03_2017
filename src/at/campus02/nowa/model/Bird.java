package at.campus02.nowa.model;

import at.campus02.nowa.interfaces.Fly;

public class Bird extends Animal implements Fly {

	public Bird(String name, String color, int countEyes) {
		super(name, color, countEyes);
		
	}

	@Override
	public void walk() {
		System.out.println("step by step.");
		
	}

	@Override
	public void makeNoise() {
		System.out.println("piep piep");
		
	}

	@Override
	public void fly() {
		System.out.println("flying in the air");
		
	}

	@Override
	public String toString() {
		return "Bird [getName()=" + getName() + ", getColor()=" + getColor() + ", getCountEyes()=" + getCountEyes()
				+ "]";
	}
	
}
