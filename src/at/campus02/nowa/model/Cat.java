package at.campus02.nowa.model;

public class Cat extends Animal {

	public Cat(String name, String color, int countEyes) {
		super(name, color, countEyes);
	}

	@Override
	public void walk() {
		System.out.println("running  ...");

	}

	@Override
	public void makeNoise() {
		System.out.println("Miauuu  ...");

	}

	@Override
	public String toString() {
		return "Bird [getName()=" + getName() + ", getColor()=" + getColor() + ", getCountEyes()=" + getCountEyes()
				+ "]";
	}
}
