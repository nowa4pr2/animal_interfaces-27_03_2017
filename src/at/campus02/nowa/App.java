package at.campus02.nowa;

import at.campus02.nowa.model.Animal;
import at.campus02.nowa.model.Bird;
import at.campus02.nowa.model.Cat;
import at.campus02.nowa.model.Frog;

public class App {
	public static void main(String[] argss) {
		
		Animal frog = new Frog("Frog","green", 2);
		Animal cat = new Cat("Cat","gray", 2);
		Bird bird = new Bird("bird","green", 2);
		
		
		System.out.println("Animal  name:" + frog.getName() + ",  color:" +frog.getColor() + ",  countEye: "  + frog.getCountEyes());
		System.out.println("Animal  name:" + cat.getName() + ",  color:" +cat.getColor() + ",  countEye: "  + cat.getCountEyes());
		System.out.println("Animal  name:" + bird.getName() + ",  color:" +bird.getColor() + ",  countEye: "  + bird.getCountEyes() + " and this animal can fly ");
		bird.fly();
	}
}
